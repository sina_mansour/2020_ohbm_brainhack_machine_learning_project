% This is a sample script for a basic linear regression model
clear
close all

% Load the train and test data
% xlsread.m works on PC but not Mac
% For Mac users, try the solution provided below
fprintf('Loading the train data...\n')
fprintf('Cortical thickness\n')
T = readtable('./data/train_cortical_thickness.csv');
train_cortical_thickness_df = table2array(T(:,3:end));

fprintf('Functional connectivity\n')
system('gunzip ./data/train_functional_connectivity.csv.gz')
T = readtable('./data/train_functional_connectivity.csv','PreserveVariableNames',true);
train_functional_connectivity_df = table2array(T(:,3:end));

fprintf('Structural connectivity\n')
system('gunzip ./data/train_structural_connectivity.csv.gz')
T = readtable('./data/train_structural_connectivity.csv','PreserveVariableNames',true);
train_structural_connectivity_df = table2array(T(:,3:end));

fprintf('Age\n')
T = readtable('./data/train_age.csv');
train_age_df = table2array(T(:,3));

fprintf('Fluid intelligence\n')
T = readtable('./data/train_fluid_intelligence.csv');
train_fluid_intelligence_df = table2array(T(:,3));

fprintf('\nLoading the test data...\n')
fprintf('Cortical thickness\n')
T = readtable('./data/test_cortical_thickness.csv');
test_cortical_thickness_df = table2array(T(:,3:end));

fprintf('Functional connectivity\n')
system('gunzip ./data/test_functional_connectivity.csv.gz')
T = readtable('./data/test_functional_connectivity.csv','PreserveVariableNames',true);
test_functional_connectivity_df = table2array(T(:,3:end));

fprintf('Structural connectivity\n')
system('gunzip ./data/test_structural_connectivity.csv.gz')
T = readtable('./data/test_structural_connectivity.csv','PreserveVariableNames',true);
test_structural_connectivity_df = table2array(T(:,3:end));

% Implement the simplest prediction model: a linear regression (this naive method is expected to overfit)
Ntest=size(test_cortical_thickness_df,1);
yfit=zeros(Ntest,2);

% Train
x_train = train_cortical_thickness_df;
% Or combine all feature x_train=[train_cortical_thickness_df,train_functional_connectivity_df,train_structural_connectivity_df];

% Test
x_test = test_cortical_thickness_df;
% Or Combine all features x_test = [test_cortical_thickness_df,test_functional_connectivity_df,test_structural_connectivity_df];

% Age prediction
% glmfit automatically adds a constant column of ones as regressor of mean  
y_train = train_age_df;
[~,~,STATS] = glmfit(x_train,y_train); % Estimate beta coefficients

yfit(:,1)= glmval(STATS.beta,x_test,'identity');

% Fluid intelligence prediction 
y_train = train_fluid_intelligence_df;
[~,~,STATS] = glmfit(x_train,y_train); % Estimate beta coefficients

yfit(:,2) = glmval(STATS.beta,x_test,'identity'); 

% Convert to table
T = array2table(yfit,'VariableNames',{'age','fluid_intelligence'});
writetable(T,'/data/test_predicitons.csv') % Write out a csv file











