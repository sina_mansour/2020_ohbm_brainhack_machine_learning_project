

![](images/logo.svg)

# 2020 OHBM Brainhack machine learning project



---

## Project overview:

Emerging evidence suggests the outstanding utility of multimodal neuroimaging data in predicting individual variation in age and cognition. However, it is unclear whether the predictive utility of such measures is a global effect or is consistently attributed to certain localized patterns of brain structure and connectivity.

This project aims to evaluate and develop an analysis framework for optimal performance of age and behavior (e.g. fluid intelligence) prediction using measures of brain structure and connectivity. The goal is to achieve both overall high prediction accuracy in cross-validated samples and high consistency in the estimated predictors (e.g. regions/connections) if possible. A secondary aim of the project is to investigate the potential markers of sub-clinical cognitive decline through simultaneous prediction of age and cognition in a healthy adult cohort.



## Project description:

Specifically, we provide processed data with extracted features of regional cortical thickness as well as whole-brain structural and functional connectivity information. The data is split into a train and test cohort such that the two cohorts are genetically unrelated. The variables of interest (age and fluid intelligence) are provided only for the training sample. The potential confounds of gender, chronological age (only for fluid intelligence), head motion (mean frame-wise displacement), and  intracranial brain volume (ICV) are regressed out of the provided variables of interest. The task at hand is to predict age and fluid intelligence for the test cohort.



## Project outcomes:

We aim to create a collection of different machine learning methods used by researchers in the field. Up to 3 researchers can form a team & work during the OHBM Brainhack 2020 to design prediction models. Each team can submit up to 5 different models for evaluation. The submitted models will later be reviewed and well-performing models will be selected for an in-depth review. Teams submitting the selected out-performing models will be contacted to get involved for potential collaborations.

The models will be evaluated based on prediction accuracy. A post hoc analyses will evaluate the consistency of estimated predictors in each model. Furthermore, the relationship between age and fluid intelligence prediction will be investigated.



---

## Data:

### Training cohort:

The feature space to be used for training is provided as 3 separate files:

- **Cortical thickness**: The mean cortical thickness of 360 brain regions for individuals is provided as a csv file: [`data/train_cortical_thickness.csv`](data/train_cortical_thickness.csv)
- **Functional connectivity**: The upper triangle of whole-brain functional connectivity is vectorized and provided as a compressed csv file. The information is the edge weights from region-to-region Pearson's correlation of resting state fMRI BOLD activity. You may binarise / threshold this feature prior to using: [`data/train_functional_connectivity.csv.gz`](data/train_functional_connectivity.csv.gz)
- **Structural connectivity**: The upper triangle of whole-brain structural connectivity is vectorized and provided as a compressed csv file. This information is the number of connected streamlines between pairs of regions. You may threshold / transform this feature prior to model fitting: [`data/train_structural_connectivity.csv.gz`](data/train_structural_connectivity.csv.gz)

*Note*: The training features provided are not normalised.

As well as the features provided above, we have provided the family information of the training set to be used in cross-validation data split: [`data/train_family.csv`](data/train_family.csv)

For the training data, measures of age and fluid intelligence are provided to fit a model:

- **Age**: Residuals of a general linear model regressing confounds of gender, motion, and ICV from chronological age is provided in a csv file as the variable of interest for age to be predicted: [`data/train_age.csv`](data/train_age.csv)
- **Fluid intelligence**: Residuals of a general linear model regressing confounds of gender, motion, chronological age, and ICV from fluid intelligence is provided in a csv file as the variable of interest for cognition to be predicted: [`data/train_fluid_intelligence.csv`](data/train_fluid_intelligence.csv)

### Testing cohort:

Similar to the training cohort, cortical thickness, functional, and structural connectivity information of a genetically unrelated cohort is provided.

- [`data/test_cortical_thickness.csv`](data/test_cortical_thickness.csv)
- [`data/train_functional_connectivity.csv.gz`](data/train_functional_connectivity.csv.gz)
- [`data/train_structural_connectivity.csv.gz`](data/train_structural_connectivity.csv.gz)



---

## Expected output:

Teams are expected to submit their predictions of age and cognition (csv files named `test_age.csv`, and `test_fluid_intelligence.csv`) in a compressed zip file. You are encouraged to also include your code, and a brief description of the model used in your zip file submission. The predictions should be submitted by creating a **confidential issue** to this repository. Make sure to include all the required information from the submission checklist. Teams can submit up to 5 different predictions for further evaluation.



---

## Where to start:

### Step 1: Clone this repository

First and foremost, clone this repository to have access to the provided data. More information provided in [this issue](https://gitlab.com/sina_mansour/2020_ohbm_brainhack_machine_learning_project/-/issues/1).

### Step 2: Join our mattermost channel

To chat about any questions regarding the project make sure to join our chat channel at: [mattermost.brainhack.org/brainhack/channels/brainhack-ml](https://mattermost.brainhack.org/brainhack/channels/brainhack-ml)

### Step 3: Register your team

You can register your team by filling this [team registration template](https://gitlab.com/sina_mansour/2020_ohbm_brainhack_machine_learning_project/-/issues/new?issuable_template=register_team) and submitting it as an issue.

### Step 4: Start coding :)

We have provided sample jupyter (python), and MATLAB scripts to load the provided data and start coding. These snippets are meant to ease the process of participating in the Brainhack project:

- Jupyter notebook: [`python_template.ipynb`](python_template.ipynb)
- MATLAB script: [`matlab_template.m`](matlab_template.m)

### Step 5: Submit your predictions

You can submit your predictions by filling this [submission template](https://gitlab.com/sina_mansour/2020_ohbm_brainhack_machine_learning_project/-/issues/new?issuable_template=submit_prediction) and submitting it as **a confidential issue**.



---

## Contact:

For any enquiry regarding the project, prediction models, and submission process. You can reach out to us from our mattermost channel:

---

### Project leads:

Sina Mansour L., Ye Tian, & Andrew Zalesky